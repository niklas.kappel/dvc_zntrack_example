# DVC + ZnTrack example

Compared to the DVC only example, we can see the following changes:

1. The stages in `stages` are no longer Python scripts, but instead Python modules that define node classes. Consider the comments in `minimize.py` for an explanation on how nodes work.
1. The workflow is defined in `main.py` by creating and connecting nodes.
1. All DVC related files are generated automatically by running `python main.py`. Only the Python files have to be written manually.
1. The node outputs are in the automatically generated `nodes` directory.

A ZnTrack node acts as a "single source of truth", there is no need to specify dependency files or parameters in multiple locations. The same node class can easily be used multiple times in a workflow by constructing multiple instances with different constructor arguments. File organization is taken care of automatically, it is enough to specify the names of output files.

Since ZnTrack nodes are regular Python classes, they can easily be packaged together and published on Git or PyPI. Then, they can be installed using pip for use in other projects or by other users.
