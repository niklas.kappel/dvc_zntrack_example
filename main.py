# pyright: reportCallIssue=false

import zntrack

from stages.minimize import Minimize
from stages.plot_populations import PlotPopulations
from stages.run_md import RunMD


def main():
    # A project defines a workflow as connected nodes.
    project = zntrack.Project()

    with project:
        # Pass external file dependencies and parameters to the node constructor.
        minimize = Minimize(
            big_data_file="data/very_large_data_file.txt",
            units="lj",
            mass_1=1.0,
            mass_2=2.0,
        )
        # Pass the output of the previous node to the next node to create a worklow graph.
        md = RunMD(
            in_configuration=minimize.configuration,
            timestep=0.005,
            step_count=30000,
        )
        PlotPopulations(
            population1vstime=md.populations_1,
            population2vstime=md.populations_2,
        )

    # Create all DVC files.
    project.build()


if __name__ == "__main__":
    main()
