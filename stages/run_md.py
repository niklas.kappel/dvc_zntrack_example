# pyright: reportAssignmentType=false
import zntrack
from lammps import PyLammps


class RunMD(zntrack.Node):
    in_configuration: str = zntrack.deps()
    timestep: float = zntrack.params()
    step_count: int = zntrack.params()

    trajectory: str = zntrack.outs_path(zntrack.nwd / "md.lammpstrj")
    thermo_data: str = zntrack.outs_path(zntrack.nwd / "md_thermo.dat")
    populations_1: str = zntrack.outs_path(zntrack.nwd / "md_population1vstime.dat")
    populations_2: str = zntrack.outs_path(zntrack.nwd / "md_population2vstime.dat")
    out_configuration: str = zntrack.outs_path(zntrack.nwd / "md.data")

    def run(self):
        lmp = PyLammps()

        # 1) Initialization
        lmp.units("lj")
        lmp.dimension("3")
        lmp.atom_style("atomic")
        lmp.pair_style("lj/cut 2.5")
        lmp.boundary("p p p")

        # 2) System definition
        lmp.read_data(self.in_configuration)
        lmp.region("region_cylinder_in cylinder z 0 0 5 INF INF side in")
        lmp.group("group_type_1 type 1")
        lmp.group("group_type_2 type 2")

        # 3) Simulation settings
        lmp.neigh_modify("every 1 delay 0 check no")

        # 4) Visualization
        output_delay = 1000

        # Thermo output
        lmp.thermo(output_delay)
        lmp.thermo_style("custom step temp press pe ke etotal")
        lmp.thermo_modify("norm no")

        # Write atom-wise data.
        lmp.dump(
            "my_dump all custom",
            output_delay,
            self.trajectory,
            "id type x y z fx fy fz",
        )

        # Write thermo data.
        thermo_file = self.thermo_data
        lmp.variable("step equal step")
        lmp.variable("pe equal pe")
        lmp.print('"# step pe"', "file", thermo_file, "screen no")
        lmp.fix(
            "my_print all print",
            output_delay,
            '"${step} ${pe}"',
            "append",
            thermo_file,
            "screen no",
        )

        # Write cylinder population data.
        lmp.variable("number_type1_in equal count(group_type_1,region_cylinder_in)")
        lmp.variable("number_type2_in equal count(group_type_2,region_cylinder_in)")
        lmp.fix(
            "myat1 all ave/time 10 200 2000 v_number_type1_in file",
            self.populations_1,
        )
        lmp.fix(
            "myat2 all ave/time 10 200 2000 v_number_type2_in file",
            self.populations_2,
        )

        # 5) Run
        lmp.velocity("all create 1.0 4928459 mom yes rot yes dist gaussian")
        lmp.fix("mynve all nve")
        lmp.fix("mylgv all langevin 1.0 1.0 0.1 1530917 zero yes")
        lmp.timestep(self.timestep)
        lmp.run(self.step_count)
        lmp.write_data(self.out_configuration)
