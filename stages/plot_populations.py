# pyright: reportAssignmentType=false
import matplotlib.pyplot as plt
import pandas as pd
import zntrack


def pick_lines(file_name, line_numbers):
    with open(file_name) as file:
        lines = [line for i, line in enumerate(file) if i in line_numbers]
    return lines


def read_col_names(file_name, line_number):
    header_line = pick_lines(file_name, [line_number])[0]
    header = header_line[1:].strip().split()
    return header


def read_population_file(file_name):
    names = read_col_names(file_name, line_number=1)
    population = pd.read_csv(file_name, comment="#", sep=r"\s+", names=names)
    return population


class PlotPopulations(zntrack.Node):
    population1vstime: str = zntrack.deps()
    population2vstime: str = zntrack.deps()

    plot: str = zntrack.outs_path(zntrack.nwd / "populations.png")

    def run(self):
        population_1 = read_population_file(self.population1vstime)
        population_2 = read_population_file(self.population2vstime)
        fig, ax = plt.subplots()
        ax.plot(
            population_1.iloc[:, 0],
            population_1.iloc[:, 1],
            label="pop1",
            linestyle="dotted",
        )
        ax.plot(
            population_2.iloc[:, 0],
            population_2.iloc[:, 1],
            label="pop2",
            linestyle="dotted",
        )
        ax.set_xlabel("Time")
        ax.set_ylabel("Population")
        ax.legend()
        fig.tight_layout()
        fig.savefig(self.plot)
