# pyright: reportAssignmentType=false
import zntrack
from lammps import PyLammps


class Minimize(zntrack.Node):
    # Declare file and parameter dependencies.
    # They will later be passed to the node constructor.
    big_data_file: str = zntrack.deps_path()
    units: str = zntrack.params()
    mass_1: float = zntrack.params()
    mass_2: float = zntrack.params()

    # Declare output files.
    # They will later be available as node attributes.
    # `zntrack.nwd` is an automatically created directory for the node.
    trajectory: str = zntrack.outs_path(zntrack.nwd / "min.lammpstrj")
    configuration: str = zntrack.outs_path(zntrack.nwd / "min.data")

    # Outputs are not limited to files. They can be any Python object.
    # Here, we declare a list of integers as an output.
    # It will be serialized and saved to a file automatically.
    numbers: list[int] = zntrack.outs()

    # The run method defines the node's task.
    # Note that we can refer to the file dependencies, parameter dependencies,
    # and output files via the node attributes defined above.
    def run(self):
        self.numbers = [1, 2, 3, 4, 5]

        # Read the large data file.
        with open(self.big_data_file) as f:
            print(f.read())

        lmp = PyLammps()

        # 1) Initialization
        lmp.units(self.units)
        lmp.dimension("3")
        lmp.atom_style("atomic")
        lmp.pair_style("lj/cut 2.5")
        lmp.boundary("p p p")

        # 2) System definition
        lmp.region("simulation_box block -10 10 -10 10 -10 10")
        lmp.create_box("2 simulation_box")
        lmp.region("region_cylinder_in cylinder z 0 0 5 INF INF side in")
        lmp.region("region_cylinder_out cylinder z 0 0 5 INF INF side out")
        lmp.create_atoms("1 random 128 341341 region_cylinder_out")
        lmp.create_atoms("2 random 16 127569 region_cylinder_in")

        # 3) Simulation settings
        lmp.mass("1", self.mass_1)
        lmp.mass("2", self.mass_2)
        lmp.pair_coeff("1 1 1.0 1.0")
        lmp.pair_coeff("2 2 0.5 3.0")
        lmp.neigh_modify("every 1 delay 0 check yes")

        # 4) Visualization
        lmp.thermo("10")
        lmp.thermo_style("custom step temp press pe ke etotal")
        lmp.dump("my_dump all atom 10", self.trajectory)

        # 5) Run
        lmp.minimize("1.0e-4 1.0e-6 1000 10000")
        lmp.write_data(self.configuration)
